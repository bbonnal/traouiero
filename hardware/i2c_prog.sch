EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 4xxx:4066 U?
U 1 1 5FF01163
P 3800 1500
AR Path="/5FF01163" Ref="U?"  Part="1" 
AR Path="/5FEFB15B/5FF01163" Ref="U12"  Part="1" 
F 0 "U12" H 3800 1327 50  0000 C CNN
F 1 "4066" H 3800 1236 50  0000 C CNN
F 2 "Package_SO:TSSOP-14_4.4x5mm_P0.65mm" H 3800 1500 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/cd4066b.pdf" H 3800 1500 50  0001 C CNN
	1    3800 1500
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4066 U?
U 2 1 5FF01169
P 3800 2500
AR Path="/5FF01169" Ref="U?"  Part="2" 
AR Path="/5FEFB15B/5FF01169" Ref="U12"  Part="2" 
F 0 "U12" H 3800 2327 50  0000 C CNN
F 1 "4066" H 3800 2236 50  0000 C CNN
F 2 "Package_SO:TSSOP-14_4.4x5mm_P0.65mm" H 3800 2500 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/cd4066b.pdf" H 3800 2500 50  0001 C CNN
	2    3800 2500
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4066 U?
U 3 1 5FF0116F
P 3800 3500
AR Path="/5FF0116F" Ref="U?"  Part="3" 
AR Path="/5FEFB15B/5FF0116F" Ref="U12"  Part="3" 
F 0 "U12" H 3800 3327 50  0000 C CNN
F 1 "4066" H 3800 3236 50  0000 C CNN
F 2 "Package_SO:TSSOP-14_4.4x5mm_P0.65mm" H 3800 3500 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/cd4066b.pdf" H 3800 3500 50  0001 C CNN
	3    3800 3500
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4066 U?
U 4 1 5FF01175
P 3800 4500
AR Path="/5FF01175" Ref="U?"  Part="4" 
AR Path="/5FEFB15B/5FF01175" Ref="U12"  Part="4" 
F 0 "U12" H 3800 4327 50  0000 C CNN
F 1 "4066" H 3800 4236 50  0000 C CNN
F 2 "Package_SO:TSSOP-14_4.4x5mm_P0.65mm" H 3800 4500 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/cd4066b.pdf" H 3800 4500 50  0001 C CNN
	4    3800 4500
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4066 U?
U 5 1 5FF0117B
P 3750 6000
AR Path="/5FF0117B" Ref="U?"  Part="5" 
AR Path="/5FEFB15B/5FF0117B" Ref="U12"  Part="5" 
F 0 "U12" H 3980 6046 50  0000 L CNN
F 1 "4066" H 3980 5955 50  0000 L CNN
F 2 "Package_SO:TSSOP-14_4.4x5mm_P0.65mm" H 3750 6000 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/cd4066b.pdf" H 3750 6000 50  0001 C CNN
	5    3750 6000
	1    0    0    -1  
$EndComp
Text HLabel 2500 1500 0    50   Input ~ 0
sda_prog
Text HLabel 2500 2500 0    50   Input ~ 0
scl_prog
Text HLabel 2500 3500 0    50   Input ~ 0
rst_prog
Text HLabel 2500 1000 0    50   Input ~ 0
prog_en
Text HLabel 5050 1500 2    50   Input ~ 0
sda_target
Text HLabel 5000 2500 2    50   Input ~ 0
scl_target
Text HLabel 5000 3500 2    50   Input ~ 0
rst_target
Text HLabel 5000 4500 2    50   Input ~ 0
pwr_target
Text HLabel 2500 4500 0    50   Input ~ 0
prog_connected
Wire Wire Line
	2500 1500 3500 1500
Wire Wire Line
	2500 2500 3500 2500
Wire Wire Line
	2500 3500 3500 3500
Wire Wire Line
	2500 4500 3500 4500
Wire Wire Line
	4100 1500 5050 1500
Wire Wire Line
	4100 2500 5000 2500
Wire Wire Line
	4100 3500 5000 3500
Wire Wire Line
	4100 4500 5000 4500
Wire Wire Line
	2500 1000 3150 1000
Wire Wire Line
	3800 1000 3800 1200
Connection ~ 3150 1000
Wire Wire Line
	3150 1000 3800 1000
Wire Wire Line
	3150 2000 3800 2000
Wire Wire Line
	3800 2000 3800 2200
Wire Wire Line
	3150 1000 3150 2000
Wire Wire Line
	3150 2000 3150 3000
Wire Wire Line
	3150 3000 3800 3000
Wire Wire Line
	3800 3000 3800 3200
Connection ~ 3150 2000
Wire Wire Line
	3150 3000 3150 4200
Wire Wire Line
	3150 4200 3800 4200
Connection ~ 3150 3000
$Comp
L power:GND #PWR035
U 1 1 5FF0F5BE
P 3750 6500
F 0 "#PWR035" H 3750 6250 50  0001 C CNN
F 1 "GND" H 3755 6327 50  0000 C CNN
F 2 "" H 3750 6500 50  0001 C CNN
F 3 "" H 3750 6500 50  0001 C CNN
	1    3750 6500
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5FE3DE5F
P 4300 5650
AR Path="/5FE3DE5F" Ref="C?"  Part="1" 
AR Path="/5FEFB15B/5FE3DE5F" Ref="C43"  Part="1" 
F 0 "C43" H 4415 5696 50  0000 L CNN
F 1 "100nF" H 4415 5605 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4338 5500 50  0001 C CNN
F 3 "~" H 4300 5650 50  0001 C CNN
	1    4300 5650
	1    0    0    -1  
$EndComp
$Comp
L power:VDD #PWR?
U 1 1 5FE3DE65
P 4300 5500
AR Path="/5FE3DE65" Ref="#PWR?"  Part="1" 
AR Path="/5FEFB15B/5FE3DE65" Ref="#PWR034"  Part="1" 
F 0 "#PWR034" H 4300 5350 50  0001 C CNN
F 1 "VDD" H 4300 5650 50  0000 C CNN
F 2 "" H 4300 5500 50  0001 C CNN
F 3 "" H 4300 5500 50  0001 C CNN
	1    4300 5500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5FE3DE6B
P 4300 5800
AR Path="/5FE3DE6B" Ref="#PWR?"  Part="1" 
AR Path="/5FEFB15B/5FE3DE6B" Ref="#PWR036"  Part="1" 
F 0 "#PWR036" H 4300 5550 50  0001 C CNN
F 1 "GND" H 4305 5627 50  0000 C CNN
F 2 "" H 4300 5800 50  0001 C CNN
F 3 "" H 4300 5800 50  0001 C CNN
	1    4300 5800
	1    0    0    -1  
$EndComp
Connection ~ 4300 5500
Wire Wire Line
	3750 5500 4300 5500
$EndSCHEMATC
